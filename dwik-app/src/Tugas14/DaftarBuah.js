import React, { useState, useEffect } from 'react';
import axios from "axios";

const DaftarBuah = () => {
    const [dataHargaBuah, setdataHargaBuah] = useState(null);
    const [inputName, setinputName] = useState("");
    const [inputHarga, setinputHarga] = useState("");
    const [inputBerat, setinputBerat] = useState("");
    const [indexofForm, setindexOfForm] = useState(-1);
    const [selectedid, setselectedid] = useState(0);
    const [statusform, setstatusform] = useState("create");

    useEffect(() => {
        if (dataHargaBuah === null) {
            axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
                .then(res => {
                    // console.log(res.data);
                    setdataHargaBuah(res.data.map(el => { return { id: el.id, nama: el.name, harga: el.price, berat: el.weight } }));
                })
        }

    }, [dataHargaBuah])



    const handleChange1 = (event) => {
        console.log(event.target.value)
        setinputName(event.target.value);
    }
    const handleChange2 = (event) => {
        console.log(event.target.value)
        setinputHarga(event.target.value);
    }
    const handleChange3 = (event) => {
        console.log(event.target.value)
        setinputBerat(event.target.value);
    }

    const handleDelete = (event) => {
        event.preventDefault()
        let nama = event.target.value
        let harga = "";
        let berat = "";
        let index = -1
        console.log(dataHargaBuah)
        let newData = [];

        for (let i = 0; i < dataHargaBuah.length; i++) {
            if (dataHargaBuah[i]["nama"] == nama) {
                harga = dataHargaBuah[i]["harga"];
                berat = dataHargaBuah[i]["berat"] * 1000;
                index = i;
            } else {
                newData.push(dataHargaBuah[i]);
            }
        }

        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${index}`)
            .then(res => {
                console.log(res);
                console.log(res.data);
            })

        // let newData = this.state.dataHargaBuah.splice(index, 1);
        setdataHargaBuah(newData);

    }

    const handleEdit = (event) => {
        event.preventDefault()
        let nama = event.target.value
        let harga = "";
        let berat = "";
        let index = -1
        console.log(dataHargaBuah)

        for (let i = 0; i < dataHargaBuah.length; i++) {
            if (dataHargaBuah[i]["nama"] == nama) {
                harga = dataHargaBuah[i]["harga"];
                berat = dataHargaBuah[i]["berat"] * 1000;
                index = i;
            }
        }
        setinputName(nama);
        setinputHarga(harga);
        setinputBerat(berat);
        setindexOfForm(index);
        setstatusform("edit");
        setindexOfForm(index)

    }

    const handleSubmit = (event) => {
        event.preventDefault()
        console.log(inputName)

        let newData = dataHargaBuah;
        newData[indexofForm] = { "nama": inputName, "harga": inputHarga, "berat": inputBerat }

        if (indexofForm === -1) {

            newData = [...dataHargaBuah, { "nama": inputName, "harga": inputHarga, "berat": inputBerat }]

        }

        if (statusform === "create") {
            axios.post(`http://backendexample.sanbercloud.com/api/fruits`, { "nama": inputName, "harga": inputHarga, "berat": inputBerat })
                .then(res => {
                    console.log(res);
                })

            setindexOfForm(-1);
            setinputName("");
            setinputHarga("");
            setinputBerat("");
            setdataHargaBuah(newData);
        } else if (statusform === "edit") {
            axios.put(`http://backendexample.sanbercloud.com/api/fruits/${indexofForm}`, { "nama": inputName, "harga": inputHarga, "berat": inputBerat })
                .then(res => {
                    console.log(res);
                })

            setindexOfForm(-1);
            setinputName("");
            setinputHarga("");
            setinputBerat("");
            setdataHargaBuah(newData);
        }

        setstatusform("create")

    }


    return (
        <>
            <h1 style={{ textAlign: "center" }}>Tabel Harga Buah</h1>
            <table style={{ border: "1px solid", width: "40%", margin: "auto", }}>
                <thead style={{ backgroundColor: "#aaa" }}>

                    <th>Nama</th>

                    <th>Harga</th>

                    <th>Berat</th>

                    <th>Action</th>

                </thead>

                <tbody style={{ backgroundColor: "coral" }}>
                    {dataHargaBuah !== null && dataHargaBuah.map(el, index => {
                        return (
                            <>

                                <tr key={el.nama}>
                                    <td>{el.nama}</td>

                                    <td>{el.harga}</td>

                                    <td>{el.berat / 1000} kg</td>

                                    <td>
                                        <button style={{ backgroundColor: "white" }} onClick={handleEdit} value={el.nama}>Edit</button>

                                        <button style={{ backgroundColor: "white" }} onClick={handleDelete} value={el.nama}>Delete</button>

                                    </td>
                                </tr>

                            </>
                        )
                    })}
                </tbody>
            </table>
            <br />
            <br />
            <h1 style={{ textAlign: "center" }}>Form Peserta</h1>
            <form onSubmit={handleSubmit}>
                <label>
                    Masukkan Nama:
                    </label>
                <input type="text" value={inputName} onChange={handleChange1} />
                <br />
                <label>
                    Masukkan Harga:
                    </label>
                <input type="text" value={inputHarga} onChange={handleChange2} />
                <br />
                <label>
                    Masukkan Berat (dalam satuan kilo):
                    </label>
                <input type="text" value={inputBerat} onChange={handleChange3} />
                <br />
                <button>submit</button>
            </form>

        </>
    )
}

export default DaftarBuah;