// const { Component } = require("react");

import React, { Component } from 'react';
import './DaftarBuah.css';

class DaftarBuah extends Component {
    render() {
        let dataHargaBuah = [
            { nama: "Semangka", harga: 10000, berat: 1000 },
            { nama: "Anggur", harga: 40000, berat: 500 },
            { nama: "Strawberry", harga: 30000, berat: 400 },
            { nama: "Jeruk", harga: 30000, berat: 1000 },
            { nama: "Mangga", harga: 30000, berat: 500 }
        ]
        return (
            <>
                <h1 style={{ textAlign: "center" }}>Tabel Harga Buah</h1>
                <table style={{ border: "1px solid", width: "40%", margin: "auto", }}>
                    <thead style={{ backgroundColor: "#aaa" }}>

                        <th>Nama</th>

                        <th>Harga</th>

                        <th>Buah</th>

                    </thead>

                    <tbody style={{ backgroundColor: "coral" }}>
                        {dataHargaBuah.map(el => {
                            return (
                                <Itembuah item={el} />
                            )
                        })}
                    </tbody>
                </table>

            </>
        )
    }
}

class Itembuah extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <>
                <tr>
                    <td>{this.props.item.nama}</td>

                    <td>{this.props.item.harga}</td>

                    <td>{this.props.item.berat / 1000} kg</td>
                </tr>

            </>
        )
    }
}

export default DaftarBuah;