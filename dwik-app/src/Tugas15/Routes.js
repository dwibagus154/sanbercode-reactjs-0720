import React from "react";
import Tugas11 from '../Tugas11/DaftarBuah'
import Timer from "../Tugas12/App";
import Tugas13 from "../Tugas13/DaftarBuah";
import Tugas14 from "../Tugas14/DaftarBuah1";
import Databuah from "./Databuah"
import { Switch, Link, Route } from "react-router-dom";

const Routes = () => {

    return (
        <>
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/tugas11">tugas11</Link>
                </li>
                <li>
                    <Link to="/tugas13">tugas13</Link>
                </li>
                <li>
                    <Link to="/tugas14">tugas14</Link>
                </li>
                <li>
                    <Link to="/tugas14">tugas14</Link>
                </li>
                <li>
                    <Link to="/databuah">Databuah</Link>
                </li>

            </ul>
            <Switch>
                <Route exact path="/">
                    <Timer />
                </Route>
                <Route exact path="/tugas11">
                    <Tugas11 />
                </Route>
                <Route path="/tugas13">
                    <Tugas13 />
                </Route>
                <Route exact path="/tugas14">
                    <Tugas14 />
                </Route>
                <Route exact path="/databuah">
                    <Databuah />
                </Route>
            </Switch>
        </>
    );
};

export default Routes;