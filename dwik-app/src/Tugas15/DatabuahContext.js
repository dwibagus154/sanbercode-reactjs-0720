import React, { useState, createContext, useEffect } from "react";
import axios from "axios"

export const DatabuahContext = createContext();

export const DatabuahProvider = props => {
    // const [movie, setMovie] = useState([
    //     { name: "Harry Potter", lengthOfTime: 120 },
    //     { name: "Sherlock Holmes", lengthOfTime: 125 },
    //     { name: "Avengers", lengthOfTime: 130 },
    //     { name: "Spiderman", lengthOfTime: 124 },
    // ]);
    const [daftarBuah, setDaftarBuah] = useState(null)
    // const [input, setInput] = useState({ name: "", price: "", weight: 0 })
    // const [selectedId, setSelectedId] = useState(0)
    // const [statusForm, setStatusForm] = useState("create")
    useEffect(() => {
        if (daftarBuah === null) {
            axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
                .then(res => {
                    setDaftarBuah(res.data.map(el => { return { id: el.id, name: el.name, price: el.price, weight: el.weight } }))
                })
        }
    }, [daftarBuah])

    return (
        <DatabuahContext.Provider value={[daftarBuah, setDaftarBuah]}>
            {props.children}
        </DatabuahContext.Provider>
    );
};