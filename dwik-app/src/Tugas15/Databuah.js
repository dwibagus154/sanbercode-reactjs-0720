import React from "react"
import { DatabuahProvider } from "./DatabuahContext";
import DatabuahList from "./DatabuahList"
import DatabuahForm from "./DatabuahForm"

const Databuah = () => {
    return (
        <DatabuahProvider>
            <DatabuahList />
            <DatabuahForm />
        </DatabuahProvider>
    )
}

export default Databuah