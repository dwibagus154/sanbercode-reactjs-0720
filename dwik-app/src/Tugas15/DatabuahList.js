import React, { useContext, useEffect, useState } from "react"
import { DatabuahContext } from "./DatabuahContext"
import axios from "axios"

const DatabuahList = () => {
    const [input, setInput] = useState({ name: "", price: "", weight: 0 })
    const [selectedId, setSelectedId] = useState(0)
    const [statusForm, setStatusForm] = useState("create")
    const [daftarBuah, setDaftarBuah] = useContext(DatabuahContext);
    // const [daftarBuah] = useContext(DatabuahContext)

    const handleDelete = (event) => {
        let idDataBuah = parseInt(event.target.value)

        let newdaftarBuah = daftarBuah.filter(el => el.id != idDataBuah)

        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idDataBuah}`)
            .then(res => {
                console.log(res)
            })

        setDaftarBuah([...newdaftarBuah])

    }

    const handleEdit = (event) => {
        let idDataBuah = parseInt(event.target.value)
        let dataBuah = daftarBuah.find(x => x.id === idDataBuah)
        setInput({ name: dataBuah.name, price: dataBuah.price, weight: dataBuah.weight })
        setSelectedId(idDataBuah)
        setStatusForm("edit")
    }

    return (
        <>
            <h1>Daftar Harga Buah</h1>
            <table>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        daftarBuah !== null && daftarBuah.map((item, index) => {
                            return (
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td>{item.name}</td>
                                    <td>{item.price}</td>
                                    <td>{item.weight / 1000} Kg</td>
                                    <td>
                                        <button onClick={handleEdit} value={item.id}>Edit</button>
                                         &nbsp;
                                        <button onClick={handleDelete} value={item.id}>Delete</button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </>
    )

}

export default DatabuahList