import React from 'react';
import logo from './logo.svg';
import { BrowserRouter as Router } from "react-router-dom";
import DaftarBuah from './Tugas14/DaftarBuah1';
import './App.css';
import Timer from './Tugas12/App';
import Contoh from './Tugas13/contoh';
import Routes from './Tugas15/Routes'

function App() {
  return (
    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.js</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a>
    //   </header>
    // </div>
    <Router>
      <div>
        <Routes />
      </div>
    </Router>
  );
}

export default App;
