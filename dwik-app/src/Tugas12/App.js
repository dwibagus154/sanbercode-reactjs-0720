import React, { Component } from 'react';



var tgl = new Date();
var jam = tgl.getHours();
var menit = tgl.getMinutes();
var detik = tgl.getSeconds();


class Timer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            time: 100
        }
    }

    componentDidMount() {
        if (this.props.start !== undefined) {
            this.setState({ time: this.props.start })
        }
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );


    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            time: this.state.time - 1
        });
    }


    render() {
        return (
            <>
                {this.state.time >= 0 && <div style={{ justifyContent: "space-between", display: "flex", paddingRight: "30px", paddingLeft: "30px" }}>
                    <h3 style={{}}>
                        sekarang jam: {jam}:{menit}:{detik}AM
                     </h3>

                    <h3 style={{}}>
                        hitung mundur: {this.state.time}
                    </h3>
                </div>}
            </>
        )
    }
}

export default Timer