// const { Component } = require("react");

import React, { Component } from 'react';

class DaftarBuah extends Component {

    constructor(props) {
        super(props)
        this.state = {
            dataHargaBuah: [
                { nama: "Semangka", harga: 10000, berat: 1000 },
                { nama: "Anggur", harga: 40000, berat: 500 },
                { nama: "Strawberry", harga: 30000, berat: 400 },
                { nama: "Jeruk", harga: 30000, berat: 1000 },
                { nama: "Mangga", harga: 30000, berat: 500 }
            ],
            inputName: "",
            inputHarga: "",
            inputBerat: "",
            indexofForm: -1
        }

        this.handleChange1 = this.handleChange1.bind(this);
        this.handleChange2 = this.handleChange2.bind(this);
        this.handleChange3 = this.handleChange3.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleChange1(event) {
        console.log(event.target.value)
        this.setState({ inputName: event.target.value });
    }
    handleChange2(event) {
        console.log(event.target.value)
        this.setState({ inputHarga: event.target.value });
    }
    handleChange3(event) {
        console.log(event.target.value)
        this.setState({ inputBerat: event.target.value });
    }

    handleDelete(event) {
        event.preventDefault()
        let nama = event.target.value
        let harga = "";
        let berat = "";
        let index = -1
        console.log(this.state.dataHargaBuah)
        let newData = [];

        for (let i = 0; i < this.state.dataHargaBuah.length; i++) {
            if (this.state.dataHargaBuah[i]["nama"] == nama) {
                harga = this.state.dataHargaBuah[i]["harga"];
                berat = this.state.dataHargaBuah[i]["berat"] * 1000;
                index = i;
            } else {
                newData.push(this.state.dataHargaBuah[i]);
            }
        }
        // let newData = this.state.dataHargaBuah.splice(index, 1);
        this.setState({ dataHargaBuah: newData });

    }

    handleEdit(event) {
        event.preventDefault()
        let nama = event.target.value
        let harga = "";
        let berat = "";
        let index = -1
        console.log(this.state.dataHargaBuah)

        for (let i = 0; i < this.state.dataHargaBuah.length; i++) {
            if (this.state.dataHargaBuah[i]["nama"] == nama) {
                harga = this.state.dataHargaBuah[i]["harga"];
                berat = this.state.dataHargaBuah[i]["berat"] * 1000;
                index = i;
            }
        }

        this.setState({
            inputName: nama,
            inputHarga: harga,
            inputBerat: berat,
            indexofForm: index
        })

    }


    handleSubmit(event) {
        event.preventDefault()
        console.log(this.state.inputName)

        let newData = this.state.dataHargaBuah;
        newData[this.state.indexofForm] = { "nama": this.state.inputName, "harga": this.state.inputHarga, "berat": this.state.inputBerat }

        if (this.state.indexofForm === -1) {

            newData = [...this.state.dataHargaBuah, { "nama": this.state.inputName, "harga": this.state.inputHarga, "berat": this.state.inputBerat }]

        }

        this.setState({
            dataHargaBuah: newData,
            inputName: "",
            inputHarga: "",
            inputBerat: ""
        })
    }

    render() {
        return (
            <>
                <h1 style={{ textAlign: "center" }}>Tabel Harga Buah</h1>
                <table style={{ border: "1px solid", width: "40%", margin: "auto", }}>
                    <thead style={{ backgroundColor: "#aaa" }}>

                        <th>Nama</th>

                        <th>Harga</th>

                        <th>Berat</th>

                        <th>Action</th>

                    </thead>

                    <tbody style={{ backgroundColor: "coral" }}>
                        {this.state.dataHargaBuah.map(el => {
                            return (
                                <>

                                    <tr key={el.nama}>
                                        <td>{el.nama}</td>

                                        <td>{el.harga}</td>

                                        <td>{el.berat / 1000} kg</td>

                                        <td>
                                            <button style={{ backgroundColor: "white" }} onClick={this.handleEdit} value={el.nama}>Edit</button>

                                            <button style={{ backgroundColor: "white" }} onClick={this.handleDelete} value={el.nama}>Delete</button>

                                        </td>
                                    </tr>

                                </>
                            )
                        })}
                    </tbody>
                </table>
                <br />
                <br />
                <h1 style={{ textAlign: "center" }}>Form Peserta</h1>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Masukkan Nama:
                    </label>
                    <input type="text" value={this.state.inputName} onChange={this.handleChange1} />
                    <br />
                    <label>
                        Masukkan Harga:
                    </label>
                    <input type="text" value={this.state.inputHarga} onChange={this.handleChange2} />
                    <br />
                    <label>
                        Masukkan Berat (dalam satuan kilo):
                    </label>
                    <input type="text" value={this.state.inputBerat} onChange={this.handleChange3} />
                    <br />
                    <button>submit</button>
                </form>

            </>
        )
    }
}






export default DaftarBuah;