var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

// Lanjutkan code untuk menjalankan function readBooksPromise 
var times = 10000;
i = 0;

panggil = (time) => {
    if (time > books[i].timeSpent) {
        i += 1;
        readBooksPromise(time, books[i])
            .then((time) => panggil(time))
            .catch((time) => panggil(time))
    } else {
        console.log('selesai')
    }
}

readBooksPromise(times, books[i])
    .then((time) => panggil(time))
    .catch((time) => panggil(time))
