// soal 1
const luas = (r) => {
    console.log(22 * r * r / 7)
}

const keliling = (r) => {
    console.log(44 * r / 7)
}

luas(7);
keliling(7);

// soal 2
let kalimat = "";

var tambah = (nama) => {
    kalimat += `${nama} `
}
tambah("saya")
tambah("adalah")
tambah("seorang")
tambah("frontend")
tambah("developer")

console.log(kalimat)

//  soal 3 

class Book {
    constructor(name, totalPage, price) {
        this.name = name;
        this.totalPage = totalPage;
        this.price = price;
    }
}

class komik extends Book {
    constructor() {
        super();
        this.isColorful = true;
    }
}