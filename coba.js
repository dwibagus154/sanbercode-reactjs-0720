let warna = ["biru", "merah", "kuning", "hijau"]

let databukutambahan = {
    penulis: "John doe",
    tahunTerbit: 2020
}

let buku = {
    nama: "pemrograman dasar",
    jumlahHalaman: 172,
    warnaSampul: ["hitam"]
}

let combinedWarna = [...warna, ...buku.warnaSampul]
console.log(combinedWarna)

let newBook = { ...buku, ...databukutambahan }
console.log(newBook)

let volumeBalok = ([...rest]) => {
    return (rest[0] * rest[1] * rest[2])
}

console.log(`volumenya adalah ${volumeBalok([3, 4, 5])}`)

let volumeKubus = ([...rest]) => {
    return (rest[0] * rest[0] * rest[0])
}

console.log(`volumenya adalah ${volumeKubus([3])}`)

let pesertaLomba = [["Budi", "Pria", "172cm"], ["Susi", "Wanita", "162cm"], ["Lala", "Wanita", "155cm"], ["Agung", "Pria", "175cm"]]
let object = []

for (i = 0; i < pesertaLomba.length; i++) {
    var peserta = {};
    peserta.nama = pesertaLomba[i][0];
    peserta.jk = pesertaLomba[i][1];
    peserta.tinggi = pesertaLomba[i][2];
    object.push(peserta);

}

console.log(object)


class BangunDatar {
    constructor(nama) {
        this.nama = nama;
    }
    get luas() {
        return ""
    }
    set luas(x) {
        this.nama = x
    }
    get keliling() {
        return ""
    }
    set keliling(x) {
        this.nama = x
    }

}

class Lingkaran {
    constructor(jari) {
        this.jari = jari
    }
    get luas() {
        return 22 * this.jari * this.jari / 7
    }

    set luas(x) {
        this.jari = x;
    }
    get keliling() {
        return 44 * this.jari / 7
    }
    set keliling(x) {
        this.jari = x;
    }
}

class Persegi {
    constructor(sisi) {
        this.sisi = sisi
    }
    get luas() {
        return this.sisi * this.sisi
    }

    set luas(x) {
        this.sisi = x;
    }


    get keliling() {
        return 4 * this.sisi
    }

    set keliling(x) {
        this.sisi = x
    }
}


lingkaran1 = new Lingkaran(7)
persegi1 = new Persegi(5)
bangun = new BangunDatar("lingkaran")

console.log(lingkaran1.luas)
console.log(lingkaran1.keliling)
console.log(persegi1.luas)
console.log(persegi1.keliling)
console.log(bangun.luas)
console.log(bangun.keliling)

function filterBooksromise(colorful, amountofPage) {
    return new Promise((resolve, reject) => {
        var books = [
            { name: "shinchan", totalPage: 50, isColorful: true },
            { name: "Kalkulus", totalPage: 250, isColorful: false },
            { name: "doraemon", totalPage: 40, isColorful: true },
            { name: "algoritma", totalPage: 300, isColorful: false },
        ]
        if (amountofPage > 0) {
            resolve(books.filter(x => x.totalPage >= amountofPage && x.isColorful == colorful))
        } else {
            var season = new Error("maaf parameter salah")
            reject(season);
        }
    })
}

filterBooksromise(false, 30)
    .then((x) => console.log(x))
    .catch((x) => console.log(x))


