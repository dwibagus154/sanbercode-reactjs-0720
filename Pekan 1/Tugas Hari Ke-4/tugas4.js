// soal 1 
console.log('LOOPING PERTAMA');
var a = 2;
while (a <= 20){
	console.log(`${a} - I love coding`)
	a+=2;
}
console.log('LOOPING KEDUA');
while (a >= 4){
	a-=2;
	console.log(`${a} - I will become a frontend developer`)
}

// soal 2

var a = 1; 
while(a <=20){
	if (a%2 == 0){
		console.log(`${a} - Berkualitas`)
	}else{
		if (a%3==0){
			console.log(`${a} - I Love Coding`)
		}else {
			console.log(`${a} - Santai`)
		}
	}
	a+=1;
}

// soal 3 

for (let a = 0 ; a < 7 ; a++){
	var n = '';
	for (let b = 0 ; b < a+1; b++){
		n += '#';
	}
	console.log(n)
}

// soal 4 
var kalimat="saya sangat senang belajar javascript";

var nama = kalimat.split(' ');
console.log(nama);

// soal 5 

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var nama = daftarBuah.sort();
for (let i = 0; i < daftarBuah.length; i++){
	console.log(nama[i]);

}